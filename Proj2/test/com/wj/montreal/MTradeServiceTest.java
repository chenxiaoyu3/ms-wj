package com.wj.montreal;

import org.junit.Assert;
import org.junit.Test;
import static org.mockito.Mockito.*;

/**
 * MTradeService Tester.
 */
public class MTradeServiceTest {

    Product mockProduct1;
    Product mockProduct2;
    Product mockProduct3;
    ProductPricingService mockPricingService;

    public MTradeServiceTest(){
        mockProduct1 = new Stock("product1", "sticker1", "exchange1");
        mockProduct2 = new Stock("product2", "sticker2", "exchange1");
        mockProduct3 = new Future("product3", "exchange1", "code1", 1, 1);


        mockPricingService = mock(ProductPricingService.class);
        when(mockPricingService.price("exchange1", "sticker1")).thenReturn(1.0);
        when(mockPricingService.price("exchange1", "sticker2")).thenReturn(2.0);
        when(mockPricingService.price("exchange1", "code1", 1, 1)).thenReturn(3.0);


    }

    @Test(expected=ProductAlreadyRegisteredException.class)
    public void testAddNewProduct_alreadyRegistered() throws Exception {
        MTradeService tradeService = new MTradeService(mockPricingService);

        Product p = new Stock("product1", "sticker1", "exchange1");
        tradeService.addNewProduct(p);
        tradeService.addNewProduct(p);
    }


    @Test
    public void testTotalTradeQuantityForDay() throws Exception {
        MTradeService tradeService = new MTradeService(mockPricingService);
        tradeService.addNewProduct(mockProduct1);
        tradeService.trade(mockProduct1, 10);
        tradeService.trade(mockProduct1, 10);
        tradeService.trade(mockProduct2, 10);
        Assert.assertEquals(20, tradeService.totalTradeQuantityForDay());

    }

    @Test
    public void testTotalValueOfAllTraded() throws Exception {
        MTradeService tradeService = new MTradeService(mockPricingService);
        tradeService.addNewProduct(mockProduct1);
        tradeService.addNewProduct(mockProduct2);
        tradeService.addNewProduct(mockProduct3);
        tradeService.trade(mockProduct1, 10);
        tradeService.trade(mockProduct2, 20);
        tradeService.trade(mockProduct3, 30);
        tradeService.trade(mockProduct3, 30);

        double expectValue = 10 * 1.0 + 20 * 2.0 + 30 * 3.0 + 30 * 3.0;
        Assert.assertEquals(expectValue, tradeService.totalValueOfAllTraded(), 0.001);
    }


} 
