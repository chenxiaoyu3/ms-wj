package com.wj.montreal;

public class Stock extends Product {
    String sticker;
    String exchange;

    public Stock(String pId, String sticker, String exchange){
        this.setProductId(pId);
        this.sticker = sticker;
        this.exchange = exchange;
    }
}
