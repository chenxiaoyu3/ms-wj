package com.wj.montreal;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class MTradeService implements MontrealTradedProducts {
    private Map<String, Product> products;
    private Map<String, Integer> booked;
    private Optional<ProductPricingService> pricingService;

    public MTradeService(ProductPricingService pricingService){
        products = new HashMap<>();
        booked = new HashMap<>();
        this.pricingService = Optional.ofNullable(pricingService);
    }


    public void addNewProduct(Product product) throws ProductAlreadyRegisteredException {
        if (products.containsKey(product.getProductId())){
            throw new ProductAlreadyRegisteredException();
        }
        products.put(product.getProductId(), product);
    }

    public void trade(Product product, int quantity) {
        if(products.containsKey(product.getProductId())){
            booked.put(product.getProductId(),
                    booked.getOrDefault(product.getProductId(), 0) + quantity);
        }
    }

    public int totalTradeQuantityForDay() {
        return booked.values().stream().mapToInt(Integer::intValue).sum();
    }

    public double totalValueOfAllTraded() {
        if(!pricingService.isPresent()){
            // or throw exception
            return 0.0;
        }
        Stream<Map.Entry<String, Integer>> trades = booked.entrySet().stream();
        Stream<Double> prices = trades.map( (entry) -> {
            String productId = entry.getKey();
            Integer productNum = entry.getValue();
            double price = 0;
            // productId in "booked" have been checked that it is in the "products"
            Product p = products.get(productId);
            if (p instanceof Stock){
                Stock s = (Stock)p;
                price = pricingService.get().price(s.exchange, s.sticker);
            }else if (p instanceof Future){
                Future f = (Future)p;
                price = pricingService.get().price(f.exchange, f.contractCode, f.month, f.year);
            }
            return price * productNum;
        });
        return prices.mapToDouble(Double::doubleValue).sum();
    }
}
