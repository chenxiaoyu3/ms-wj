package com.wj.montreal;

public class Future extends Product {
    String exchange;
    String contractCode;
    int month;
    int year;

    public Future(String pId, String exchange, String contractCode, int month, int year){
        this.setProductId(pId);
        this.exchange = exchange;
        this.contractCode = contractCode;
        this.month = month;
        this.year = year;
    }

}
