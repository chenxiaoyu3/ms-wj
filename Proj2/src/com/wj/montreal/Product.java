package com.wj.montreal;

public abstract class Product {
    private String productId;

    public String getProductId(){
        return productId;
    }
    public void setProductId(String productId){
        this.productId = productId;
    }
}
