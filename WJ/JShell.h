//
// Created by JieWei on 8/22/16.
//

#ifndef UNIXAPI_JSHELL_H
#define UNIXAPI_JSHELL_H

#include <string>
#include <map>
#include <vector>

#include "cmds.h"

using namespace std;

class JShell {

    const char* _promot = "JShell > ";

    // begin, handle commands with pipe
    int handleCommands(string line);
    // handle command with >, 2>
    int handleCommand(string command);
    // handle commmand with built in
    int handleCommand(vector<string> segs, string input, string& output1, string& output2);
    // exec
    int single_exec(vector<string> segs, string input, string& output1, string& output2);
    std::map<string, cmd_type> buildin_cmds;

    void log(string str);

public:
    JShell();
    void execute();
};


#endif //UNIXAPI_JSHELL_H

