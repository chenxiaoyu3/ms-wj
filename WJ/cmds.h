//
// Created by JieWei on 8/22/16.
//

#ifndef UNIXAPI_CMDS_H
#define UNIXAPI_CMDS_H

#include <iostream>
using namespace std;

typedef int (*cmd_type)(const char*, string&);

int echo(const char* argv, string& output);

int cd(const char *argv, string& output);

int cmd1(const char *argv, string& output);

#endif //UNIXAPI_CMDS_H
