#include <iostream>

#include "JShell.h"

class FF{
public:


    int operator()(int i){
        return i;
    }
};
int main(int argc, char* argv[]) {
//    std::cout << "Hello, JShell!" << std::endl;
//    JShell jShell;
//    jShell.execute();
    int x = 1;
    auto bar = [&]{++x;};
    bar();
    cout << x;
    return 0;
}