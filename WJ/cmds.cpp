//
// Created by JieWei on 8/22/16.
//
// cmds.cpp contains commmands like binary in /usr/bin

#include <iostream>
#include <unistd.h>

#include "cmds.h"

int cd(const char *argv, string& output) {
    int ret = chdir(argv);
    output = string("I'm cd, i'm going to ") + argv;
    return ret;
}

int echo(const char* argv, string& output) {
    output = string(argv) + "\n";
    return 0;
}

int cmd1(const char* argv, string& output) {
    output = "cmd1... do nothing.\n";
    return 0;
}

// add more built in commands here...