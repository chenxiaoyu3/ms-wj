#include <iostream>
#include "String.h"

using namespace std;
int main() {

    String thread, twine, rope("Hello");
    thread = "world";
    thread += "\n";
    rope += " ";
    twine = rope + thread;

    if (twine(0, 4) == String("Hell"))
        cout << "Heaven" << endl;

    twine[6] = 'W';
    cout << twine<< endl;
    const String hang("one two three");
    cout << twine << endl;
    cout << hang[2] << endl;

    String test1('a');
    test1 = "1234";
    cout << (const char*)test1 << endl;
    return 0;
}