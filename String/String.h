#ifndef STRING_STRING_H
#define STRING_STRING_H


class String {
    char* str;
public:
    // constructor
    String();
    String(const char);
    String(const char*);
    String(const char*, unsigned, unsigned);
    String(const String &);
    String(String &&);
    virtual ~String();

    // converter
    operator const char*() const;

    // function
    size_t length() const;
    const char* c_str() const;
    String substr(unsigned, unsigned) const;
    String& append(const char);
    String& append(const char*);
    String& append(const String&);

    // operator
    String& operator=(const char);
    String& operator=(const String&);
    String& operator=(String&&);
    String operator+(const char) const;
    String operator+(const char*) const;
    String operator+(const String&) const;
    String& operator+=(const char);
    String& operator+=(const char*);
    String& operator+=(const String&);
    bool operator==(const String&) const;
    bool operator==(const char*) const;

    char& operator[](const unsigned);
    const char& operator[](const unsigned) const;
    String operator()(unsigned, unsigned) const;


};


#endif //STRING_STRING_H
