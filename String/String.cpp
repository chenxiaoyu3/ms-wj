#include <cstring>
#include <algorithm>
#include <assert.h>
#include "String.h"

// constructor
String::String() {
    str = new char[1];
    str[0] = 0;
}

String::String(const char c) {
    str = new char[2];
    str[0] = c;
    str[1] = 0;
}

String::String(const char *s) {
    str = new char[std::strlen(s) + 1];
    std::strcpy(str, s);
}

String::String(const char *s, unsigned begin, unsigned length) {
    str = new char[length + 1];
    std::strncpy(str, s + begin, length);
    str[length] = 0;
}

String::String(const String &s) {
    str = new char[s.length() + 1];
    std::strcpy(str, s.str);
}

String::String(String &&s) : str(s.str) {
    s.str = nullptr;
}

String::~String() {
    delete[] str;
}

// converter
String::operator const char *() const {
    return c_str();
}

// function
size_t String::length() const {
    return std::strlen(str);
}

const char *String::c_str() const {
    return str;
}

String &String::append(const char c) {
    auto size = length();
    char *s2 = new char[size + 2];
    std::strcpy(s2, str);
    s2[size] = c;
    s2[size + 1] = 0;
    delete[] str;
    str = s2;
    return *this;
}

String &String::append(const char *s) {
    auto size = length();
    char *s2 = new char[size + std::strlen(s) + 1];
    std::strcpy(s2, str);
    std::strcpy(s2 + size, s);
    delete[] str;
    str = s2;
    return *this;
}

String &String::append(const String &s) {
    return append(s.str);
}


// operator
String &String::operator=(const String &s) {
    String temp(s);
    std::swap(temp.str, str);
    return *this;
}

String &String::operator=(String &&r) {
    std::swap(r.str, str);
    return *this;
}

String &String::operator=(const char c) {
    String temp(c);
    std::swap(temp.str, str);
    return *this;
}

String String::operator+(const char *s) const {
    String temp(str);
    temp.append(s);
    return temp;
}

String String::operator+(const String &s) const {
    return (*this) + s.str;
}

String String::operator+(const char c) const {
    String temp(str);
    temp.append(c);
    return temp;
}

const char &String::operator[](const unsigned pos) const {
    assert(pos < length());
    return str[pos];
}

char &String::operator[](const unsigned pos) {
    assert(pos < length());
    return str[pos];;
}

bool String::operator==(const char *s) const {
    return std::strcmp(str, s) == 0;
}

bool String::operator==(const String &s) const {
    return (*this) == s.str;
}

String String::substr(unsigned begin, unsigned len) const {
    assert(begin + len < length());
    return String(str, begin, len);
}

String String::operator()(unsigned begin, unsigned len) const {
    return substr(begin, len);
}

String &String::operator+=(const char *s) {
    return append(s);
}

String &String::operator+=(const String &s) {
    return append(s);
}

String &String::operator+=(const char c) {
    return append(c);
}
